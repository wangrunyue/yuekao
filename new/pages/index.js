import Head from "next/head";
import Link from "next/link";
import styles from "../styles.module.css";
import Home from "./home";
import About from "./about";
import Serve from "./serve";
const list = [
  "首页",
  "关于",
  "业务与服务",
  "新闻中心",
  "投资作者关系",
  "联系我们",
  "加入我们",
  "简/EN",
];

export default () => (
  <div>
    <Head>
      <title>陆金所控股有限公司</title>
      <meta charSet="utf-8" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      <link
        rel="icon"
        href="https://static.lufaxcdn.com/lufaxholding/common/images/favicon.b53519c6.png"
      ></link>
      {/* <link rel="stylesheet" href="https://static.lufaxcdn.com/lufaxholding/css/lufax/index.77907958.css"></link> */}
    </Head>
    <div className={styles.title}>
      <h1>
        <img
          src="https://static.lufaxcdn.com/lufaxholding/common/images/logo.80d5324a.jpg"
          alt="lufax"
        />
      </h1>
      <div>
        {list.map((item) => {
          return <span>{item}</span>;
        })}
      </div>
    </div>
    <div className={styles.content}>
      <video
        width="100%"
        id="video"
        x-webkit-airplay="true"
        airplay="allow"
        webkit-playsinline="true"
        playsinline="true"
        src="https://ljs1.wmcn.com/static/upload/video/20201026/16037095810705273.mp4"
        class="video1 video"
        autoplay=""
        loop=""
        preload=""
        muted=""
      ></video>
      <div class={styles.home_index_title_container}>
        <h1>我们的使命</h1>

        <div class={styles.home_index_subtitle}>
          让零售信贷和财富管理更简单，更安全，更高效。
        </div>
        <div className={styles.two}>
          <div class="home_index_block_content_title_container">
            <img
              src="https://static.lufaxcdn.com/lufaxholding/pages/lufax/images/index_errand_1_title.e6ae3db3.png"
              alt=""
            />
            <div class="home_index_block_content_subtitle ">
              基于深度学习和大数据分析应用，平台支持自动化组合投资工具，制定定制化且匹配投资者风险偏好的投资组合选择，能通过差异化和自动化投资助力提升投资回报。
            </div>
          </div>
          <img
            src="https://static.lufaxcdn.com/lufaxholding/pages/lufax/images/index_errand_1.d29f1660.png"
            alt=""
          ></img>
        </div>
      </div>
    </div>
  </div>
);
